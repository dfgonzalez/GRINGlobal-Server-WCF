﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Net;

namespace GrinGlobal.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WCFGRINGlobalService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WCFGRINGlobalService.svc or WCFGRINGlobalService.svc.cs at the Solution Explorer and start debugging.
    // [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class WCFGRINGlobalService : IWCFGRINGlobalService
    {
        [WebInvoke(Method = "GET", UriTemplate = "getdata?dataviewname={dataviewName}&parameters={parameters}&token={token}")]
        public Stream getdata(string token, string dataviewName, string parameters)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"];

                using (SecureData sd = new SecureData(false, httpToken))
                {
                    DataSet ds = sd.GetData(dataviewName, parameters, 0, 0, "");
                    if (ds.Tables.Contains(dataviewName))
                    {
                        JSONString = JsonConvert.SerializeObject(ds.Tables[dataviewName]);
                    }
                    else
                    {
                        JSONString = JsonConvert.SerializeObject(ds.Tables["ExceptionTable"]);
                    }
                }
            }
            catch (Exception e)
            {
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Status Login(Credential credential)
        {
            var status = new Status();
            try
            {
                DataSet dsLogin = SecureData.TestLogin(true, credential.Username, credential.Password);
                if (dsLogin.Tables.Contains("validate_login"))
                {
                    status.Token = SecureData.Login(credential.Username, credential.Password);
                    if (string.IsNullOrEmpty(status.Token))
                    {
                        status.Success = false;
                        status.Error = "Error generating token.";
                    }
                    else
                    {
                        status.Success = true;
                    }
                }
                else {
                    status.Success = false;
                    status.Error = "Invalid user name / password combination.";
                }
            }
            catch (Exception ex)
            {
                status.Success = false;
                status.Error = ex.Message;
            }

            return status;
        }

        public Stream ReadData(string tablename, string id)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            var entity = new Dictionary<string, object>();
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_" + tablename, ":" + tablename + "id= " + id, 0, 0, "");
                    var table = dataset.Tables["table_" + tablename];
                    var row = table.AsEnumerable().First();

                    foreach (DataColumn c in table.Columns)
                    {
                        entity.Add(c.ColumnName, row.Field<object>(c.ColumnName));
                    }

                    JSONString = JsonConvert.SerializeObject(entity);
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream CreateData(string tablename, string data)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var entity = JObject.Parse(data);
                var entity2 = JsonConvert.DeserializeObject(data);

                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_" + tablename, ":" + tablename + "id=-1", 0, 0, "");
                    var table = dataset.Tables["table_" + tablename];
                    var row = table.NewRow();

                    foreach (DataColumn c in table.Columns)
                    {
                        JToken prop = entity[c.ColumnName];
                        if (prop != null && prop.Type != JTokenType.Null && !prop.HasValues)
                            row.SetField(c, prop);
                    }
                    table.Rows.Add(row);

                    DataSet dsChanges = new DataSet();
                    dsChanges.Tables.Add(table.GetChanges());
                    var dsError = sd.SaveData(dsChanges, true, "");

                    if (dsError.Tables.Contains("table_" + tablename)) {
                        JSONString = JsonConvert.SerializeObject(dsError.Tables["table_" + tablename]);
                    }
                    else
                    {
                        JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);  
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream DeleteData(string tablename, string id)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var ds = sd.GetData("table_" + tablename, ":" + tablename + "id=" + id, 0, 0, "");
                    DataTable tb = null;
                    if (ds != null && ds.Tables.Contains("table_" + tablename))
                    {
                        tb = ds.Tables["table_" + tablename];
                        var nr = tb.Rows[0];
                        nr.Delete();

                        var dsError = sd.SaveData(ds, true, "");
                        if (dsError.Tables.Contains("table_" + tablename))
                        {
                            JSONString = JsonConvert.SerializeObject(dsError.Tables["table_" + tablename]);
                        }
                        else
                        {
                            JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream UpdateData(string tablename, string id, string data)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;
            var JSONString = string.Empty;

            try
            {
                var entity = JObject.Parse(data);

                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");
                using (SecureData sd = new SecureData(false, httpToken))
                {
                    var dataset = sd.GetData("table_" + tablename, ":" + tablename + "id=" + id, 0, 0, "");
                    var table = dataset.Tables["table_" + tablename];
                    var row = table.Rows[0];

                    foreach (DataColumn c in table.Columns)
                    {
                        JToken prop = entity[c.ColumnName];
                        if (prop != null && !c.ReadOnly)
                        {
                            row.SetField(c, prop);
                        }
                    }

                    var dsChanges = new DataSet();
                    dsChanges.Tables.Add(table.GetChanges());
                    var dsError = sd.SaveData(dsChanges, true, "");
                    if (dsError.Tables.Contains("table_" + tablename))
                    {
                        JSONString = JsonConvert.SerializeObject(dsError.Tables["table_" + tablename]);
                    }
                    else
                    {
                        JSONString = JsonConvert.SerializeObject(dsError.Tables["ExceptionTable"]);
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                JSONString = JsonConvert.SerializeObject(e.Message);
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }

        public Stream SearchData(string tablename, string dataview, string query)
        {
            var request = WebOperationContext.Current.IncomingRequest;
            var headers = request.Headers;

            var JSONString = string.Empty;
            try
            {
                var httpToken = headers["Authorization"].Trim().Replace("Bearer ", "");

                using (SecureData sd = new SecureData(false, httpToken))
                {
                    //           sd.Search(query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, 0, 0, null, options, null, null);
                    DataSet ds = sd.Search(query, true, true, null, tablename, 0, 1000, 0, 0, null, "", null, null);

                    if (ds != null)
                    {
                        if (ds.Tables.Contains("SearchResult"))
                        {
                            if (ds.Tables["SearchResult"].Rows.Count > 0)
                            {
                                var ids = new List<string>();
                                foreach (DataRow row in ds.Tables["SearchResult"].Rows)
                                {
                                    ids.Add(row.ItemArray[0].ToString());
                                }
                                if (string.IsNullOrEmpty(dataview))
                                {
                                    var response = sd.GetData("table_" + tablename, ":" + tablename + "id= " + string.Join(",", ids.ToArray()), 0, 0, "");
                                    JSONString = JsonConvert.SerializeObject(response.Tables["table_" + tablename]);
                                }
                                else
                                {
                                    var response = sd.GetData(dataview, ":" + tablename + "id= " + string.Join(",", ids.ToArray()), 0, 0, "");
                                    JSONString = JsonConvert.SerializeObject(response.Tables[dataview]);
                                }
                            }
                            else
                            {
                                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent; //204
                                return null;
                            }
                        }
                        else
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                            JSONString = JsonConvert.SerializeObject(ds.Tables["ExceptionTable"]);
                        }
                    }
                    else
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                        JSONString = JsonConvert.SerializeObject("Search Error");
                    }
                }
            }
            catch (Exception e)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError; //500
                JSONString = JsonConvert.SerializeObject(e.Message);
            }

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(JSONString));
        }
    }

    public class Status
    {
        public bool Success { set; get; }
        public string Error { set; get; }
        public string Token { set; get; }
    }

    public class Credential
    {
        public string Username { set; get; }
        public string Password { set; get; }
    }

}

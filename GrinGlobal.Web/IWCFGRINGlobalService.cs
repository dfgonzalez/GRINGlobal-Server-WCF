﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using System.ServiceModel.Web;

namespace GrinGlobal.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWCFGRINGlobalService" in both code and config file together.
    [ServiceContract]
    public interface IWCFGRINGlobalService
    {
        [OperationContract]
        Stream getdata(string token, string dataviewName, string parameters);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        Status Login(Credential credential);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "search/{tablename}?dataview={dataview}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream SearchData(string tablename, string dataview, string query);

        #region REST_table_dataview

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "rest/{tablename}/{id}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Stream ReadData(string tablename, string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "rest/{tablename}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        Stream CreateData(string tablename, string data);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "rest/{tablename}/{id}", ResponseFormat = WebMessageFormat.Json)]
        Stream DeleteData(string tablename, string id);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "rest/{tablename}/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream UpdateData(string tablename, string id, string data);

        #endregion
    }
}
